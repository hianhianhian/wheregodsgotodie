// Copyright 2019 hianhianhian

using UnrealBuildTool;
using System.Collections.Generic;

public class WhereGodsGoToDieTarget : TargetRules
{
	public WhereGodsGoToDieTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "WhereGodsGoToDie" } );
	}
}
