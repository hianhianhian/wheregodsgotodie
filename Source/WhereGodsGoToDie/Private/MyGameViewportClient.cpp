// Copyright 2019 hianhianhian


#include "MyGameViewportClient.h"

void UMyGameViewportClient::Init(struct FWorldContext& WorldContext, UGameInstance* OwningGameInstance, bool bCreateNewAudioDevice)
{
	Super::Init(WorldContext, OwningGameInstance, bCreateNewAudioDevice);
	// Refresh the hardware cursors, ensures that bIsMouseOverClient is set to true
	Super::MouseLeave(Viewport);
	Super::MouseEnter(Viewport, 0, 0);
}