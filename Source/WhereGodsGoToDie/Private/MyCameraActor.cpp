// Copyright 2019 hianhianhian


#include "MyCameraActor.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"

AMyCameraActor::AMyCameraActor()
{
	// This class doesn't tick, but all child classes (including BP-based ones!) will, so turn this on here.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.SetTickFunctionEnable(true);
}

void AMyCameraActor::BeginPlay()
{
	Super::BeginPlay();

	// Check the validity of Instigator

	if (Instigator)
	{
		// We always want to tick after the actor we're tracking - it avoids visual jitter.
		PrimaryActorTick.AddPrerequisite(Instigator, Instigator->PrimaryActorTick);
	}
}

/**
* Our chase cam child class implementation.
*/
AChaseCam::AChaseCam()
{
	IdealOffset.Set(4500.0f, 0.0f, 0.0f);
	FieldOfView = 20.0f;
	/*
	CameraSpeed_PerAxis.Set(800.0f, 800.0f, 400.0f);
	CameraSpeedLimiter = 800.0f;
	CameraDistanceLimiter_PerAxis.Set(300.0f, 300.0f, 200.0f);
	CameraDistanceLimiter = 800.0f;
	*/
	CameraLagSpeed = 4.0f;
	CameraLagMaxDistance = 0.f;
}

void AChaseCam::BeginPlay()
{
	Super::BeginPlay();

	GetCameraComponent()->SetFieldOfView(FieldOfView);

	if (Instigator) {
		RotatedIdealOffset = IdealOffset.RotateAngleAxis(-45.0f, FVector::RightVector);		// Rotate about the y-axis, the negative means it is 45 deg off the ground
		FVector CameraPos = Instigator->GetActorLocation() + RotatedIdealOffset;			// Add the rotated vector to the PlayerCharacter position
		SetActorLocation(CameraPos);
		GetCameraComponent()->SetWorldRotation(UKismetMathLibrary::FindLookAtRotation(RotatedIdealOffset, FVector::ZeroVector).Quaternion());
		PreviousIdealLocation = CameraPos;
		PreviousDesiredLocation = PreviousIdealLocation;
	}
}

void AChaseCam::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (Instigator)
	{
		FVector IdealLocation = Instigator->GetActorLocation() + RotatedIdealOffset;
		FVector CurrentLocation = GetActorLocation();
		FVector DesiredLocation = IdealLocation;

		// Interpolate to get the desired location for next frame based on the InterpSpeed and DeltaSeceonds
		{
			DesiredLocation = FMath::VInterpTo(PreviousDesiredLocation, DesiredLocation, DeltaSeconds, CameraLagSpeed);
		}

		bool bClamped = false;
		// Clamp distance to ensure the camera doesn't get more than CameraLagMaxDistance away from the character.
		if (CameraLagMaxDistance > 0.f)
		{
			const FVector Distance = DesiredLocation - IdealLocation;
			if (Distance.SizeSquared() > FMath::Square(CameraLagMaxDistance))
			{
				DesiredLocation = IdealLocation + Distance.GetClampedToMaxSize(CameraLagMaxDistance);
				bClamped = true;
			}
		}

		PreviousIdealLocation = IdealLocation;
		PreviousDesiredLocation = DesiredLocation;

		// Apply all movement values to the camera actor.
		SetActorLocation(DesiredLocation);

		// Debug code
#if !UE_BUILD_SHIPPING
		if (bShowDebugInfo)
		{
			if (UWorld* World = GetWorld())
			{
				FVector DebugLineOffset = FVector(-RotatedIdealOffset.X, 0.0f, -RotatedIdealOffset.Z);
				FVector NewLocation = GetActorLocation();
				DrawDebugSphere(World, IdealLocation - RotatedIdealOffset, 8.f, 16, FColor::Red);
				DrawDebugSphere(World, DesiredLocation - RotatedIdealOffset, 8.f, 16, FColor::Yellow);
				DrawDebugLine(World, CurrentLocation + DebugLineOffset, NewLocation + DebugLineOffset, bClamped ? FColor::Red : FColor::Green, false, 3.0f, 0, 3.0f);
				/*
				DrawDebugSphere(World, NewLocation + DebugLineOffset, 8.f, 16, FColor::Yellow);
				DrawDebugSphere(World, IdealLocation + DebugLineOffset, 8.f, 16, FColor::Red);
				FVector BoxVector = FVector(CameraDistanceLimiter_PerAxis.X, CameraDistanceLimiter_PerAxis.Y, 0.0f);
				DrawDebugBox(World, NewLocation + DebugLineOffset, BoxVector, FColor::Green, false, -1.0f, 1, 5.0f);
				FMatrix CircleMatrix = FMatrix(FVector::UpVector, FVector::ForwardVector, FVector::RightVector, NewLocation + DebugLineOffset);
				// Draw circles for each distance range
				for (int32 j = 0; j < SpeedAdjusters.Num(); ++j)
				{
					// Check if adjuster is polar/radial
					if (SpeedAdjusters[j].AxisFlags & (1 << (int32)EChaseCamAxes::ECCA_Radius))
					{
						DrawDebugCircle(World, CircleMatrix, SpeedAdjusters[j].DistanceRange.Y, 64, FColor::Red, false, -1.0f, 1, 4.0f);
					}
				}
				*/
			}
			if (GEngine) {
				GEngine->AddOnScreenDebugMessage(2, 2.0f, FColor::Yellow, IdealLocation.ToString());
				GEngine->AddOnScreenDebugMessage(3, 2.0f, FColor::Red, Instigator->GetActorLocation().ToString());
			}

		}
#endif
		/*
		// Old camera movement that was very jerky and jittery
		FVector IdealLocation = Instigator->GetActorLocation() + RotatedIdealOffset;
		FVector CurrentLocation = GetActorLocation();
		FVector DeltaLocation = IdealLocation - CurrentLocation;
		FVector AbsDeltaLocation = FVector(FMath::Abs(DeltaLocation.X), FMath::Abs(DeltaLocation.Y), FMath::Abs(DeltaLocation.Z));
		FVector MaxMovementThisFrame_PerAxis = CameraSpeed_PerAxis * DeltaSeconds;
		FVector DesiredMovementThisFrame;
		FVector MovementThisFrameDueToCap = FVector::ZeroVector;
		float CurrentCameraSpeedLimiter = CameraSpeedLimiter;

		// Enforces a "clamp-box" area to keep the character on screen, making sure the camera doesn't get too far behind.
		{
			// Clamp by radius.
			{
				FVector Direction;
				float Length;
				DeltaLocation.ToDirectionAndLength(Direction, Length);
				if (Length > CameraDistanceLimiter)
				{
					FVector CapVector = (Length - CameraDistanceLimiter) * Direction;
					MovementThisFrameDueToCap += CapVector;
					DeltaLocation -= CapVector;
				}
			}
			// Clamp by axis.
			for (int32 i = 0; i < 3; ++i)
			{
				if (AbsDeltaLocation.Component(i) > CameraDistanceLimiter_PerAxis.Component(i))
				{
					MovementThisFrameDueToCap.Component(i) += FMath::Sign(DeltaLocation.Component(i))
						* (AbsDeltaLocation.Component(i) - CameraDistanceLimiter_PerAxis.Component(i));
					DeltaLocation.Component(i) += FMath::Clamp(DeltaLocation.Component(i), -CameraDistanceLimiter_PerAxis.Component(i), CameraDistanceLimiter_PerAxis.Component(i));
					// Update value cached in AbsDeltaLocation
					AbsDeltaLocation.Component(i) = FMath::Abs(DeltaLocation.Component(i));
				}
			}
		}

		// Adjust camera speed based on distance, applying damping where required based on the data values given.
		{
			float DeltaLocationSize = DeltaLocation.Size();

			// Attempt a radius-based adjustment.
			for (int32 j = 0; j < SpeedAdjusters.Num(); ++j)
			{
				// Use this adjuster only if it's polar/radial and we're within its distance range.
				if ((SpeedAdjusters[j].AxisFlags & (1 << (int32)EChaseCamAxes::ECCA_Radius))
				&& (DeltaLocationSize == FMath::Clamp(DeltaLocationSize, SpeedAdjusters[j].DistanceRange.X, SpeedAdjusters[j].DistanceRange.Y)))
				{
					// Store this value for the adjustment code, which is run later during Tick.
					CurrentCameraSpeedLimiter *= FMath::GetMappedRangeValueUnclamped(SpeedAdjusters[j].DistanceRange, SpeedAdjusters[j].SpeedMultiplierRange, DeltaLocationSize);
					// Don't look any further, we've already found an acceptable limiter.
					break;
				}
			}

			// Attempt axis-based adjustments now.
			for (int32 i = (int32)EChaseCamAxes::ECCA_X; i <= (int32)EChaseCamAxes::ECCA_Z; ++i)
			{
				for (int32 j = 0; j < SpeedAdjusters.Num(); ++j)
				{
					// Use this adjuster only if it supports this axis and we're within its distance range.
					if ((SpeedAdjusters[j].AxisFlags & (1<<i))
						&& (AbsDeltaLocation.Component(i) == FMath::Clamp(AbsDeltaLocation.Component(i), SpeedAdjusters[j].DistanceRange.X, SpeedAdjusters[j].DistanceRange.Y)))
					{
						// Scale the max movement we're allowed to make (during this frame, on this axis) by the adjustment value. It will be applied later during Tick.
						MaxMovementThisFrame_PerAxis.Component(i) *= FMath::GetMappedRangeValueUnclamped(SpeedAdjusters[j].DistanceRange, SpeedAdjusters[j].SpeedMultiplierRange, AbsDeltaLocation.Component(i));
						// Dont' look any further, we've already found an acceptable limiter.
						break;
					}
				}
			}
		}


		// Manage the offset between desired and current camera position.
		{
			// Limit per-axis offset
			for (int32 i = 0; i < 3; ++i)
			{
					DesiredMovementThisFrame.Component(i) = FMath::Sign(DeltaLocation.Component(i))
						* FMath::Min(AbsDeltaLocation.Component(i), MaxMovementThisFrame_PerAxis.Component(i));
			}
			// Limit overall magnitude
			if (GEngine)
				GEngine->AddOnScreenDebugMessage(4, 2.0f, FColor::Green, FString::SanitizeFloat(CurrentCameraSpeedLimiter));
			DesiredMovementThisFrame = DesiredMovementThisFrame.GetClampedToMaxSize(CurrentCameraSpeedLimiter * DeltaSeconds);
		}

		// Apply all movement values to the camera actor.
		SetActorLocation(CurrentLocation + DesiredMovementThisFrame + MovementThisFrameDueToCap);
		*/
	}
}
