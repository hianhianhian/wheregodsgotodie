// Copyright 2019 hianhianhian

#pragma once

#include "Core.h"
#include "Engine/GameViewportClient.h"
#include "MyGameViewportClient.generated.h"

/**
 * 
 */
UCLASS()
class UMyGameViewportClient : public UGameViewportClient
{
	GENERATED_BODY()
	
public:
	void Init(FWorldContext & WorldContext, UGameInstance * OwningGameInstance, bool bCreateNewAudioDevice);

	/** The platform-specific viewport which this viewport client is attached to. */
	FViewport* Viewport;
};
