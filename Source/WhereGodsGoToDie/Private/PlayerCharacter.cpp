// Copyright 2019 hianhianhian

#include "PlayerCharacter.h"
#include "InputCoreTypes.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/CollisionProfile.h"
#include "Engine/StaticMesh.h"
#include "Engine/LocalPlayer.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Containers/UnrealString.h"
#include "DrawDebugHelpers.h"
#include "Abilities/PlayerAbilitySystemComponent.h"
#include "Abilities/MyGameplayAbility.h"
#include "MyCameraActor.h"
#include "WhereGodsGoToDie.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bShowDebugInfo = false;
	DashSpeed = 10.f;
	DashDistance = 600.f;
	DashMovementLockTime = 0.15f;

	// Create ability system component and set it to be explicitly replicated 
	AbilitySystemComponent = CreateDefaultSubobject<UPlayerAbilitySystemComponent>(TEXT("AbilitySystem"));
	AbilitySystemComponent->SetIsReplicated(true);
	// Full means every GameplayEffect is replicated to every client
	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Full);
}

UAbilitySystemComponent * APlayerCharacter::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	UWorld* World = GetWorld();

	check(World);
	Super::BeginPlay();

	if (!CameraActorToSpawn)
	{
		CameraActorToSpawn = AMyCameraActor::StaticClass();
	}
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Instigator = this;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	MyCamera = World->SpawnActor<AMyCameraActor>(CameraActorToSpawn->GetDefaultObject()->GetClass(), GetTransform(), SpawnParameters);
	// Just crash the game if the camera is not found for debug purposes
	check(MyCamera);

	//This is too late! BeginPlay has already been called on the new camera. Use SpawnParameters instead.
	// MyCamera->Instigator = this;	 

	if (APlayerController* PC = Cast<APlayerController>(GetController()))
	{
		PC->SetViewTarget(MyCamera);
	}
	
	bCanMove = true;

	// Initialise the AllKeys array with all possible EKeys
	EKeys::GetAllKeys(AllKeys);
#if !UE_BUILD_SHIPPING
	if (bShowDebugInfo) {
		for (int32 i = 0; i < AllKeys.Num(); ++i)
		{
			UE_LOG(LogTemp, VeryVerbose, TEXT("Key: %s"), *AllKeys[i].ToString());
		}
	}
#endif
}

void APlayerCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (MyCamera)
	{
		MyCamera->Destroy();
	}
	Super::EndPlay(EndPlayReason);
}

void APlayerCharacter::PossessedBy(AController * NewController)
{
	Super::PossessedBy(NewController);

	AbilitySystemComponent->RefreshAbilityActorInfo();
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	TickEvent.Broadcast(DeltaTime);

	if (APlayerController* PC = Cast<APlayerController>(GetController()))
	{	
		// Update cursor location
		{
			FString TraceString;
			if (!bIsGamepad)
			{
				FHitResult TraceResult(ForceInit);
				if (PC->GetHitResultUnderCursorByChannel(UEngineTypes::ConvertToTraceType(ECC_Visibility), true, TraceResult))
				{
					if (TraceResult.GetActor() != nullptr)
					{
						TraceString += FString::Printf(TEXT("Trace Actor: %s, "), *TraceResult.GetActor()->GetName());
					}
					CursorLocation = TraceResult.Location;
				}

				// TODO: Project from 2D screen space to 3D world space correctly
				/*{
					FVector WorldLoc;
					FVector WorldDir;
					UWorld* World = GetWorld();
					check(World);
					ULocalPlayer* const LocalPlayer = PC->GetLocalPlayer();
					if (LocalPlayer && LocalPlayer->ViewportClient)
					{
						FSceneViewFamilyContext ViewFamily(FSceneViewFamily::ConstructionValues(
							LocalPlayer->ViewportClient->Viewport,
							World->Scene,
							LocalPlayer->ViewportClient->EngineShowFlags)
							.SetRealtimeUpdate(true));

						FVector OutViewLocation;
						FRotator OutViewRotation;
						FSceneView* SceneView = LocalPlayer->CalcSceneView(&ViewFamily, OutViewLocation, OutViewRotation, LocalPlayer->ViewportClient->Viewport);
						if (SceneView != NULL)
						{
							FVector2D ScreenPosition;
							if (LocalPlayer->ViewportClient->GetMousePosition(ScreenPosition))
							{
								SceneView->DeprojectFVector2D(ScreenPosition, WorldLoc, WorldDir);
								FVector TempLoc = WorldLoc + WorldDir * 5000.f;
								if (GEngine && bShowDebugInfo) {
									GEngine->AddOnScreenDebugMessage(600, 2.0f, FColor::Green, FString::Printf(TEXT("WorldLoc: %s, WorldDir: %s"), *WorldLoc.ToString(), *WorldDir.ToString()));
									GEngine->AddOnScreenDebugMessage(601, 2.0f, FColor::Green, FString::Printf(TEXT("CursorLoc: %s"), *TempLoc.ToString()));
								}
							}
						}
					}
				}*/
			}
			else
			{
				CursorLocation = GetActorLocation();
			}
			TraceString += FString::Printf(TEXT("Cursor Location: %s"), *CursorLocation.ToString());

#if !UE_BUILD_SHIPPING
			if (GEngine && bShowDebugInfo) {
				GEngine->AddOnScreenDebugMessage(104, 2.0f, FColor::Green, TraceString);
			}
#endif
		}
	
	}
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Bind axis mappings to input functions
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);
	PlayerInputComponent->BindAxis("MoveUp", this, &APlayerCharacter::MoveUp);

	// Bind action mappings to input functions
	PlayerInputComponent->BindAction("Dash", IE_Pressed, this, &APlayerCharacter::OnBeginDash);
	PlayerInputComponent->BindAction("AnyKey", IE_Pressed, this, &APlayerCharacter::AnyKeyHandler);

	AbilitySystemComponent->BindAbilityActivationToInputComponent(PlayerInputComponent, FGameplayAbilityInputBinds(FString("ConfirmInput"), 
		FString("CancelInput"), FString("AbilityInput")));
}


/* 
* Input functions for axis mappings
*/
void APlayerCharacter::MoveRight(float AxisValue)
{
	FVector RightDirection = FVector(0.0f, -1.0f, 0.0f); 
	if (bCanMove)
	{
		if (AxisValue != 0.0f)
		{
			AddMovementInput(RightDirection, FMath::Clamp<float>(AxisValue, -1.0f, 1.0f));
		}
	}
}

void APlayerCharacter::MoveUp(float AxisValue)
{
	FVector UpDirection = FVector(-1.0f, 0.0f, 0.0f);
	if (bCanMove)
	{
		if (AxisValue != 0.0f) 
		{
			AddMovementInput(UpDirection, FMath::Clamp<float>(AxisValue, -1.0f, 1.0f));
		}
	}
}

// Handler that triggers when any key is pressed, for gamepad detection
void APlayerCharacter::AnyKeyHandler()
{
	if (APlayerController* PC = Cast<APlayerController>(GetController()))
	{
		for (int32 i = 0; i < AllKeys.Num(); ++i) {
			if (PC->WasInputKeyJustPressed(AllKeys[i]))
			{
				bIsGamepad = EKeys::IsGamepadKey(AllKeys[i]);
				PC->bShowMouseCursor = !bIsGamepad;
				if (bIsGamepad) {
					int32 screenX;
					int32 screenY;
					PC->GetViewportSize(screenX, screenY);
					PC->SetMouseLocation(screenX / 2, screenY / 2);
				}
#if !UE_BUILD_SHIPPING
				if (GEngine && bShowDebugInfo) {
					GEngine->AddOnScreenDebugMessage(102, 2.0f, FColor::Green, FString::Printf(TEXT("Key: %s, i = %d"), *AllKeys[i].ToString(), i));
					GEngine->AddOnScreenDebugMessage(103, 2.0f, FColor::Cyan, bIsGamepad ? TEXT("True") : TEXT("False"));
				}
#endif
			}

		}
	}
}

/*
* Input function for checking if Dash is allowed, binds the DashTickListener if yes.
*/
void APlayerCharacter::OnBeginDash()
{
	if (GetCharacterMovement()->IsMovingOnGround() && bCanMove)
	{
		bCanMove = false;
		PreviousDashLoc = GetActorLocation();
		FVector StartingDir; 
		FRotator StartingRot;
		GetStartingDirectionAndRotation(StartingDir, StartingRot);
		DirectionToMoveIn = PreviousDashLoc + StartingDir * DashDistance;

		// Rotate player character in dash direction
#if !UE_BUILD_SHIPPING
		if (GEngine && bShowDebugInfo) {
			GEngine->AddOnScreenDebugMessage(111, 2.0f, FColor::Cyan, FString::Printf(TEXT("Dash rotation: %s"), *StartingRot.ToString()));
		}
#endif
		SetActorRotation(StartingRot);

		// Do a sphere trace in the dash direction for blocking objects
		{
			TArray<AActor *> EmptyActors;
			FHitResult OutHit;
			// If blocking hit occurs in the direction of dashing, change dash distance to the hit location
			if (UKismetSystemLibrary::SphereTraceSingleByProfile(GetWorld(), PreviousDashLoc, DirectionToMoveIn, 42.f, FName(TEXT("IgnoreOnlyPawn")), false, EmptyActors,
				bShowDebugInfo ? EDrawDebugTrace::ForDuration : EDrawDebugTrace::None, OutHit, true, FLinearColor::Green, FLinearColor::Red, 1.0f))
			{
				// Something is blocking the actor at the location specified, log actor name and change the dash location to the sweep hit location
				DirectionToMoveIn = OutHit.Location;
				if (OutHit.GetActor() != nullptr && bShowDebugInfo) {
					UE_LOG(LogMovementDash, Verbose, TEXT("Dash is blocked by %s at location: %s"), *OutHit.GetActor()->GetName(), *OutHit.Location.ToString());
				}
			}
		}

		// Bind the DashTickListener to TickEvent to do interpolation
		{
			DashEndTime = 0.f;
			ExpectedDashDistance = (DirectionToMoveIn - PreviousDashLoc).Size();
			if (bShowDebugInfo)
			{
				UE_LOG(LogMovementDash, Verbose, TEXT("Expected distance: %f"), ExpectedDashDistance);
			}
			TickEvent.AddDynamic(this, &APlayerCharacter::DashTickListener);
		}
	}
}

/*
* Listener that responds to the TickEvent delegate. Gets bound to the delegate when dash registers, and unbound in the OnStopDash.
* Implements the interpolation through the dash distance.
*/
void APlayerCharacter::DashTickListener(float DeltaTime)
{
	// Just using a basic accumulation on DeltaTime for now, need to experiment with different means of interpolation possibly.
	AccumulatedTime += DeltaTime;
	if (DashEndTime <= 0.f)
	{
		// Move the player character towards the temp interpolation location with a little bit added to Z-axis to handle small elevation changes.
		FVector DesiredDashLoc = FMath::VInterpTo(PreviousDashLoc, DirectionToMoveIn, AccumulatedTime, DashSpeed);
		DesiredDashLoc = FVector(DesiredDashLoc.X, DesiredDashLoc.Y, DesiredDashLoc.Z + 10.f);
		SetActorLocation(DesiredDashLoc, true);

		TotalDashDistance += (DesiredDashLoc - PreviousDashLoc).Size();
		PreviousDashLoc = DesiredDashLoc;
	#if !UE_BUILD_SHIPPING
		if (bShowDebugInfo)
		{
			if (UWorld* World = GetWorld())
			{
				UE_LOG(LogMovementDash, Verbose, TEXT("DeltaTime : %f, Adjusted Time: %f, Distance travelled: %f"), DeltaTime, AccumulatedTime, TotalDashDistance);
				DrawDebugSphere(World, DesiredDashLoc, 8.f, 16, FColor::Green, false, 0.5f);
			}
		}
	#endif
	}
	// If the total dash distance is greater equal to the expected dash distance (plus minus a small margin), we've reached our destination
	if (TotalDashDistance >= ExpectedDashDistance - KINDA_SMALL_NUMBER)
	{
		// Set the final dash time once, then continue accumulating time 
		if (DashEndTime <= 0.f) {
			DashEndTime = AccumulatedTime;
#if !UE_BUILD_SHIPPING
			if (bShowDebugInfo) {
				UE_LOG(LogMovementDash, Verbose, TEXT("Dash took : %f s"), DashEndTime);
			}
#endif
		}
		else if (AccumulatedTime > DashEndTime + DashMovementLockTime)
		{
			// Once the accumulated time has passed the dash movement lock time, we call the handler for ending dash
#if !UE_BUILD_SHIPPING
			if (bShowDebugInfo) {
				UE_LOG(LogMovementDash, Verbose, TEXT("Movement locked : %f s"), AccumulatedTime - DashEndTime);
			}
#endif
			OnStopDash();
		}
	}
}

// Called when the target dash location has been achieved, unbinds DashTickListener, resets temp values.
void APlayerCharacter::OnStopDash()
{
	TickEvent.RemoveDynamic(this, &APlayerCharacter::DashTickListener);
	bCanMove = true;
	TotalDashDistance = 0.f;
	AccumulatedTime = 0.f;
}

// Get the normalised vector direction for the character to move in.
void APlayerCharacter::GetStartingDirectionAndRotation(FVector& OutputVector, FRotator& OutputRot)
{
	FVector StartingLocation = GetActorLocation();
	FVector StartingDirection;
	FString TempString;

	
	// If gamepad is detected, use player controller rotation for starting direction
	if (bIsGamepad) 
	{
		float UpAxis = GetInputAxisValue("MoveUp");
		float RightAxis = GetInputAxisValue("MoveRight");
		FRotator ControllerRot;
		// If non-zero movement, use the inverse tan of the up and right axes to get the rotation vector
		if (UpAxis != 0.f || RightAxis != 0.f)
		{
			ControllerRot = UKismetMathLibrary::MakeRotator(0, 0, UKismetMathLibrary::DegAtan2(RightAxis, UpAxis));
			StartingDirection = UKismetMathLibrary::NegateVector(UKismetMathLibrary::Conv_RotatorToVector(ControllerRot));
		}
		else
		{
			StartingDirection = GetActorForwardVector();
		}
		OutputRot = UKismetMathLibrary::Conv_VectorToRotator(StartingDirection);
	}
	// Else if mouse detected, use hit result under mouse cursor location
	else {
		FRotator CursorRot = UKismetMathLibrary::FindLookAtRotation(StartingLocation, FVector(CursorLocation.X, CursorLocation.Y, StartingLocation.Z));
		FVector ToCursor = UKismetMathLibrary::GetForwardVector(CursorRot);
		StartingDirection = FVector(ToCursor.X, ToCursor.Y, 0.f);
		OutputRot = CursorRot;
	}
	TempString += FString::Printf(TEXT("Starting Direction: %s"), *StartingDirection.ToString());
#if !UE_BUILD_SHIPPING
	if (GEngine && bShowDebugInfo) {
		GEngine->AddOnScreenDebugMessage(110, 2.0f, bIsGamepad ? FColor::Green : FColor::Cyan, TempString);
	}
#endif
	
	OutputVector = StartingDirection;
}

void APlayerCharacter::AddCharacterAbilities()
{
	// Grant abilities to character, but only on the server
	if (Role != ROLE_Authority || AbilitySystemComponent == nullptr || AbilitySystemComponent->CharacterAbilitiesGiven)
	{
		return;
	}

	for (TSubclassOf<UMyGameplayAbility>& StartupAbility : CharacterAbilities)
	{
		AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(StartupAbility, 1, INDEX_NONE, this));
	}
	AbilitySystemComponent->CharacterAbilitiesGiven = true;
}
