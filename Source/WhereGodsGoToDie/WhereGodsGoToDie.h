// Copyright 2019 hianhianhian

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogMovementDash, Verbose, All);

// The UEnum needed to translate input action names to be bound to the AbilitySystemComponent
// BlueprintType is used in case it needs to be used in blueprints
UENUM(BlueprintType)
enum class AbilityInput : uint8
{
	// 0 Jump
	Jump			UMETA(DisplayName = "Jump"),
	// 1 Dash
	Dash			UMETA(DisplayName = "Dash"),
	// 2 Swing
	Swing			UMETA(DisplayName = "Swing"),
	// 3 
	LightAttack		UMETA(DisplayName = "Light Attack"),
	// 4 RMB
	AnyKey			UMETA(DisplayName = "AnyKey"),
};