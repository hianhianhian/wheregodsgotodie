// Copyright 2019 hianhianhian

#include "WhereGodsGoToDie.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, WhereGodsGoToDie, "WhereGodsGoToDie" );
DEFINE_LOG_CATEGORY(LogMovementDash);
