// Copyright 2019 hianhianhian

#pragma once

#include "CoreMinimal.h"
#include "Engine/Engine.h"
#include "Delegates/Delegate.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "PlayerCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTickEvent, float, DeltaTime);

class AMyCameraActor;

UCLASS(Blueprintable)
class WHEREGODSGOTODIE_API APlayerCharacter : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

	// Begin Actor Interface
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void PossessedBy(AController* NewController) override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End Actor Interface

	// Implement IAbilitySystemInterface
	class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

protected:
	// Our ability system component, which we will attach to the PlayerCharacter
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities", meta = (AllowPrivateAccess = "true"))
	class UPlayerAbilitySystemComponent* AbilitySystemComponent;

	// Camera class/BP that is spawned for the PlayerCharacter.
	UPROPERTY(EditAnywhere, Category = "Camera")
	TSubclassOf<AMyCameraActor> CameraActorToSpawn;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	AMyCameraActor* MyCamera;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	uint8 bShowDebugInfo : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	uint8 bIsGamepad : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Movement")
	uint8 bCanMove : 1;

	// Input functions for axis mappings
	void MoveRight(float AxisValue);
	void MoveUp(float AxisValue);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TArray<TSubclassOf<class UMyGameplayAbility>> CharacterAbilities;

	/* 
	* Input functions to handle dash
	*/
	void OnBeginDash();
	void OnStopDash();

	/*
	* Controls how quickly the dash target position is reached.
	* Values are measured in Unreal units per second, high values = faster speed, low values = slow speed,  zero value is zero speed.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Movement", meta = (ClampMin = "0.0", ClampMax = "3000.0", UIMin = "0.0", UIMax = "3000.0"))
	float DashSpeed;

	// Amount of time before the player character can move after dash is completed.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Movement", meta = (ClampMin = "10.0", ClampMax = "2000.0", UIMin = "10.0", UIMax = "2000.0"))
	float DashDistance;

	// Amount of time before the player character can move after dash is completed.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Movement", meta = (ClampMin = "0.01", ClampMax = "5.0", UIMin = "0.01", UIMax = "5.0"))
	float DashMovementLockTime;

private:
	// Delegate listener for the tick event.
	UFUNCTION()
	void DashTickListener(float DeltaTime);

	// Temporary variables for storing information during dashing.
	FVector PreviousDashLoc;
	FVector DirectionToMoveIn;
	float ExpectedDashDistance;
	float TotalDashDistance;
	float AccumulatedTime;
	float DashEndTime;

protected:
	// Any key handler for gamepad detection
	void AnyKeyHandler();

	/*	
	*	If gamepad, get the direction that the player character is facing towards. 
	*	If mouse, get the location under the cursor and convert that from rotation to a vector.
	*/
	UFUNCTION(BlueprintCallable, Category = "Character Movement")
	void GetStartingDirectionAndRotation(FVector& OutputVector, FRotator& OutputRot);

	// Location of the cursor, if mouse is enabled. Gamepad sets the CursorLocation to the Character location.
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Default)
	FVector CursorLocation;

	// Grant abilities on the Server. The Ability Specs will be replicated to the owning client.
	virtual void AddCharacterAbilities();

private:
	// Delegate for exporting DeltaTime from Tick
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Tick")
	FTickEvent TickEvent;

	// Array of all keys, needed for checking if gamepad or M+KB
	TArray<FKey> AllKeys;

	// Temporary variable for storing the mouse location from previous frame.
	FVector PreviousMouseLoc;

};