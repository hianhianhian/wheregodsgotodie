// Copyright 2019 hianhianhian

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "PlayerSprite.generated.h"

/**
 * 
 */
UCLASS()
class WHEREGODSGOTODIE_API APlayerSprite : public APaperCharacter
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character") float PlayerHealth;
};
