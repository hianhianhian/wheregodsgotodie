// Copyright 2019 hianhianhian

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "MyGameplayAbility.generated.h"

/**
 * 
 */
UCLASS()
class WHEREGODSGOTODIE_API UMyGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()
	
public:
	UMyGameplayAbility();
};
