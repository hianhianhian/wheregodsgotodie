// Copyright 2019 hianhianhian

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "PlayerAbilitySystemComponent.generated.h"

/**
 * Custom AbilitySystemComponent in case any methods need to be extended in the future.
 */
UCLASS()
class WHEREGODSGOTODIE_API UPlayerAbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()

public:
	bool CharacterAbilitiesGiven = false;
	
};
