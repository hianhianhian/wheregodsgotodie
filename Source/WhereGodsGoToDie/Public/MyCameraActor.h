// Copyright 2019 hianhianhian
/**
* Using code from Richard Hinckley's Getting Started with Gameplay Programming stream
* https://forums.unrealengine.com/showthread.php?150726
*/

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraActor.h"
#include "MyCameraActor.generated.h"

/**
 * This is the game's base camera class.
 */
UCLASS()
class WHEREGODSGOTODIE_API AMyCameraActor : public ACameraActor
{
	GENERATED_BODY()
	
public:
	AMyCameraActor();
	virtual void BeginPlay() override;
};

/*
* Old code, not needed for now
UENUM(Meta = (Bitflags))
enum class EChaseCamAxes
{
	ECCA_X UMETA(DisplayName = "X Axis"),
	ECCA_Y UMETA(DisplayName = "Y Axis"),
	ECCA_Z UMETA(DisplayName = "Z Axis"),
	ECCA_Radius UMETA(DisplayName = "Polar (radius)"),
};

USTRUCT()
struct FChaseCamSpeedAdjustment
{
	GENERATED_BODY()

	// This speed adjuster will take effect when (this.X < CameraDesiredDistance < this.Y)
	UPROPERTY(EditAnywhere)
	FVector2D DistanceRange;

	// This multiplier will be used based on a direct mapping to camera distance's value within DistanceRange.
	UPROPERTY(EditAnywhere)
	FVector2D SpeedMultiplierRange;

	// This flags the Axis/Axes to be used, or if Radius should be used instead.
	UPROPERTY(EditAnywhere, Meta = (Bitmask, BitmaskEnum = "EChaseCamAxes"))
	int32 AxisFlags;
};
*/

/**
 * This is a child class to our base camera class.
 */
UCLASS()
class WHEREGODSGOTODIE_API AChaseCam : public AMyCameraActor
{
	GENERATED_BODY()

public:
	AChaseCam();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

protected:
	// The ideal distance that this camera will maintain from the follow target (Instigator). Implementation only handles vectors along x-axis.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ChaseCamSettings)
	FVector IdealOffset;

	// The field of view of the camera.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ChaseCamSettings, meta = (ClampMin = "5.0", ClampMax = "170.0", UIMin = "5.0", UIMax = "170.0"))
	float FieldOfView;

	/*
	* Old code, not needed for now
	// Max speed for camera adjustment, per axis.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector CameraSpeed_PerAxis;

	// Max speed for camera adjustment, all axes combined.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CameraSpeedLimiter;

	// Max distance the camera can be from its ideal location. This is per-axis because of the shape of the screen.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector CameraDistanceLimiter_PerAxis;


	// Speed adjustments. When the camera is within one of these ranges, it will be used to scale chasecam speed.
	UPROPERTY(EditAnywhere)
	TArray<FChaseCamSpeedAdjustment> SpeedAdjusters;
	*/
	// Max distance the camera can be from its ideal location. This is a radius to avoid forcing 45-degree camera tracking movement.

	// Turn this on to show debug info.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CameraLag)
	uint8 bShowDebugInfo : 1;

	// Controls how quickly camera reaches target position. Low values are slower (more lag), high values are faster (less lag), while zero is instant (no lag). 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CameraLag, meta = (ClampMin = "0.0", ClampMax = "1000.0", UIMin = "0.0", UIMax = "1000.0"))
	float CameraLagSpeed;

	// Max distance the camera target may lag behind the current location. If set to zero, no max distance is enforced. 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CameraLag, meta = (ClampMin = "0.0", UIMin = "0.0"))
	float CameraLagMaxDistance;

private:
	// The ideal offset rotated along the y-axis to achieve isometric look.
	FVector RotatedIdealOffset;

	// Temporary variable to record previous camera position for camera lag.
	FVector PreviousDesiredLocation;
	FVector PreviousIdealLocation;

};