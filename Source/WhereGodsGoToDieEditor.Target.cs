// Copyright 2019 hianhianhian

using UnrealBuildTool;
using System.Collections.Generic;

public class WhereGodsGoToDieEditorTarget : TargetRules
{
	public WhereGodsGoToDieEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "WhereGodsGoToDie" } );
	}
}
